<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $fillable = [
        'foto_categoria_id',
        'legenda',
        'arquivo',
    ];

    public function fotoCategoria()
    {
        return $this->belongsTo('App\FotoCategoria');
    }
}
