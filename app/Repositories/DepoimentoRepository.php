<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class DepoimentoRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Depoimento";
    }

    function listar()
    {
        $model = $this->model->orderBy('id', 'DESC')->paginate();
        return $model;
    }

    function ultimos($quantidade = 3)
    {
        return $this->model->limit($quantidade)->orderBy('id', 'DESC')->get();
    }
}