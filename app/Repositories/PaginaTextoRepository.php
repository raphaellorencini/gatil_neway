<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class PaginaTextoRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\PaginaTexto";
    }

    function listar()
    {
        $model = $this->model->all();
        return $model;
    }
}