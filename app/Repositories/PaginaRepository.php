<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class PaginaRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Pagina";
    }

    function listar()
    {
        $model = $this->model->all();
        return $model;
    }

    function listar_por_slug($slug)
    {
        $model = $this->model->where(['slug' => $slug])->first();
        return $model;
    }
}