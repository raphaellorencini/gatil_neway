<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class FotoCategoriaRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\FotoCategoria";
    }

    function listar($grupo, $order_by = null, $limit = null)
    {
        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = 'nome';
            }
            $model = $this->model->where(['grupo' => $grupo, 'publicado' => 1])->orderBy($order_by)->get();
        }else{
            if(empty($order_by)){
                $order_by = 'id';
            }
            $model = $this->model->where(['grupo' => $grupo, 'publicado' => 1])->orderBy($order_by)->limit($limit)->get();
        }
        return $model;
    }

    function listar_paginado($grupo, $order_by = null, $limit = null)
    {
        if(empty($order_by)){
            $order_by = ['created_at','DESC'];
        }
        $model = $this->model->whereIn('grupo',$grupo)->where(['publicado' => 1])->orderBy($order_by[0], $order_by[1])->paginate($limit);

        return $model;
    }
}