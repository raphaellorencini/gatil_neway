<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class ColaboradorRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Colaborador";
    }

    function listar()
    {
        $model = $this->model->orderBy('id', 'ASC')->paginate();
        return $model;
    }

    function ultimos($quantidade = 3)
    {
        return $this->model->limit($quantidade)->orderBy('id', 'DESC')->get();
    }
}