<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaginaTexto extends Model
{
    protected $table = 'paginas_textos';

    protected $fillable = [
        'pagina_id',
        'nome',
        'texto',
    ];
}
