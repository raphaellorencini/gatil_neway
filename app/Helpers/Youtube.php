<?php

namespace App\Helpers;


class Youtube
{
    /**
     * @param $url
     * @param bool $iframe - usar youtube com iframe ou só a url
     * @param int $width
     * @param int $height
     * @return mixed
     */
    public static function embed($url, $iframe = false, $width = 420, $height = 315)
    {
        $p = [
            "\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)",
            "\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)",
            "\s*[a-zA-Z\/\/:\.]*youtube.com\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)"
        ];
        $p = implode('|',$p);
        $pattern = "/{$p}/i";
        preg_match($pattern, $url, $matches);

        $replace = ['https://youtu.be/','https://www.youtube.com/watch?v=','https://www.youtube.com/embed/'];
        $youtube_id = str_replace($replace, '', $matches[0]);

        if($iframe) {
            $url2 = "<iframe width=\"".$width."\" height=\"".$height."\" src=\"https://www.youtube.com/embed/{$youtube_id}\" frameborder=\"0\" allowfullscreen></iframe>";
        }else{
            $url2 = "https://www.youtube.com/embed/{$youtube_id}";
        }

        return $url2;
    }

    /**
     * @param $url
     * @param string $quality - default, mq, hq
     * @return mixed
     */
    public static function thumb($url, $quality = "default", $width = 200)
    {
        switch ($quality){
            case 'mq':
                $q = 'mqdefault.jpg';
                break;
            case 'h1':
                $q = 'hqdefault.jpg';
                break;
            default:
                $q = 'default.jpg';
                break;
        }
        //$pattern = "/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i";
        $p = [
            "\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)",
            "\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)",
            "\s*[a-zA-Z\/\/:\.]*youtube.com\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)"
        ];
        $p = implode('|',$p);
        $pattern = "/{$p}/i";
        preg_match($pattern, $url, $matches);

        $replace = ['https://youtu.be/','https://www.youtube.com/watch?v=','https://www.youtube.com/embed/'];
        $youtube_id = str_replace($replace, '', $matches[0]);

        $img = "<img src='https://img.youtube.com/vi/".$youtube_id."/".$q."' style='width: ".$width."px;' \/>";

        return $img;
    }
}