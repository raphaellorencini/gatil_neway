<?php
/**
 * Created by PhpStorm.
 * User: Raphael
 * Date: 26/09/2016
 * Time: 19:16
 */

namespace App\Helpers;


class Texto
{
    public static function resumo($texto, $tamanho = 200)
    {
        $texto = strip_tags(html_entity_decode($texto));
        if(strlen($texto) > $tamanho){
            $txt = mb_substr($texto,0,$tamanho);
        }else{
            $txt = $texto;
        }
        return $txt;
    }
}