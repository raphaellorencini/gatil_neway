<?php

namespace App\Helpers;


class Thumb
{
    public static function img($url, $num = null, $largura_altura = 'w')
    {
        $u = explode('/', $url);
        $ultima_key = count($u)-1;
        $u[$ultima_key] = "thumbs/".$u[$ultima_key];

        $url = implode('/', $u);
        $style = '';
        if(!empty($num)) {
            $style = "width: {$num}px;";
            if ($largura_altura == 'h') {
                $style = "height: {$num}px;";
            }
        }
        return sprintf("<img src=\"%s\" alt=\"\" style=\"%s\">",$url, $style);
    }
}