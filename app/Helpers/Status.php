<?php

namespace App\Helpers;


class Status
{
    public static function label($val)
    {
        switch ($val){
            case 1:
                $status = "<div class=\"alert alert-info status\" role=\"alert\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i> Em análise</div>";
                break;
            case 2:
                $status = "<div class=\"alert alert-success status\" role=\"alert\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i> Concluído</div>";
                break;
            default:
                $status = "<div class=\"alert alert-warning status\" role=\"alert\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i> Pendente</div>";
        }

        return $status;
    }

    public static function label_grupo($val)
    {
        switch ($val){
            case 1:
                $status = "Matrizes";
                break;
            case 2:
                $status = "Padreadores";
                break;
            case 3:
                $status = "Disponíveis";
                break;
            case 4:
                $status = "Galeria de Fotos";
                break;
            default:
                $status = "Galeria de Fotos";
        }

        return $status;
    }

    public static function label_publicado($val)
    {
        switch ($val){
            case 1:
                $status = "Sim";
                break;
            case 2:
                $status = "Não";
                break;
            default:
                $status = "Não";
        }

        return $status;
    }
}