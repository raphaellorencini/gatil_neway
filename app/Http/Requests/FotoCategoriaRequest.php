<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FotoCategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grupo' => 'required',
            'nome' => 'required',
            'arquivo' => 'required',
            'publicado' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'grupo.required' => 'O campo Grupo é obrigatório!',
            'nome.required' => 'O campo Nome é obrigatório!',
            'arquivo.required' => 'O campo Imagem é obrigatório!',
            'publicado.required' => 'O campo Publicado é obrigatório!',
        ];
    }
}
