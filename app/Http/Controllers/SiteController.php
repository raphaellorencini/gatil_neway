<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Foto;
use App\FotoCategoria;
use App\Noticia;
use App\Repositories\BannerRepository;
use App\Repositories\ColaboradorRepository;
use App\Repositories\DepoimentoRepository;
use App\Repositories\FotoCategoriaRepository;
use App\Repositories\FotoRepository;
use App\Repositories\NoticiaRepository;
use App\Repositories\PaginaRepository;
use App\Repositories\VideoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class SiteController extends Controller
{
    /**
     * @var NoticiaRepository
     */
    private $noticiaRepository;

    /**
     * @var FotoCategoriaRepository
     */
    private $fotoCategoriaRepository;
    /**
     * @var BannerRepository
     */
    private $bannerRepository;
    /**
     * @var PaginaRepository
     */
    private $paginaRepository;
    /**
     * @var FotoRepository
     */
    private $fotoRepository;
    /**
     * @var DepoimentoRepository
     */
    private $depoimentoRepository;
    /**
     * @var ColaboradorRepository
     */
    private $colaboradorRepository;

    public function __construct
    (
        NoticiaRepository $noticiaRepository,
        FotoCategoriaRepository $fotoCategoriaRepository,
        BannerRepository $bannerRepository,
        PaginaRepository $paginaRepository,
        FotoRepository $fotoRepository,
        DepoimentoRepository $depoimentoRepository,
        ColaboradorRepository $colaboradorRepository
    )
    {
        $this->noticiaRepository = $noticiaRepository;
        $this->fotoCategoriaRepository = $fotoCategoriaRepository;
        $this->bannerRepository = $bannerRepository;
        $this->paginaRepository = $paginaRepository;
        $this->fotoRepository = $fotoRepository;
        $this->depoimentoRepository = $depoimentoRepository;
        $this->colaboradorRepository = $colaboradorRepository;
    }

    public function teste()
    {
        $teste = $this->fotoCategoriaRepository->listar(1, 2);
        foreach ($teste as $v){
            echo 'nome: '.$v->nome.'<br>';
            foreach ($v->fotos as $v2){
                echo 'arquivo: '.$v2->arquivo.'<br>';
            }
            echo '<br><br>';
        }

        $teste = $this->fotoCategoriaRepository->listar(2, 2);
        foreach ($teste as $v){
            echo 'nome: '.$v->nome.'<br>';
            foreach ($v->fotos as $v2){
                echo 'arquivo: '.$v2->arquivo.'<br>';
            }
            echo '<br><br>';
        }
        //var_dump($teste);
    }

    public function index()
    {
        $banners = $this->bannerRepository->listar();
        $matrizes = $this->fotos(1);
        $padreadores = $this->fotos(2);
        $artigos = $this->noticiaRepository->ultimos();
        $disponiveis = $this->fotoCategoriaRepository->listar(3);
        $videos = $this->fotoRepository->ultimos(12);

        $pagina = $this->paginaRepository->listar_por_slug('home');
        $texto = [];
        foreach ($pagina->textos as $v){
            $texto[] = [
                'nome' => $v->nome,
                'texto' => $v->texto
            ];
        }
        return view('site.index', compact('banners','matrizes','padreadores','artigos','disponiveis','videos','texto','videos'));
    }

    public function nossos_gatos()
    {

        $matrizes = $this->fotoCategoriaRepository->listar(1);
        $padreadores = $this->fotoCategoriaRepository->listar(2);
        $disponiveis = $this->fotoCategoriaRepository->listar(3);
        $galeria = $this->fotoCategoriaRepository->listar(4);

        return view('site.nossos_gatos',compact('matrizes','padreadores','disponiveis','galeria','artigos'));
    }

    public function disponiveis()
    {
        $disponiveis = $this->fotoCategoriaRepository->listar(3);

        $pagina = $this->paginaRepository->listar_por_slug('disponiveis');
        $texto = [];
        foreach ($pagina->textos as $v){
            $texto[] = [
                'nome' => $v->nome,
                'texto' => $v->texto
            ];
        }

        return view('site.disponiveis',compact('disponiveis', 'texto'));
    }

    public function videos()
    {
        $videos = $this->fotoRepository->listar(12);
        return view('site.videos',compact('videos'));
    }

    public function sobre()
    {
        $pagina = $this->paginaRepository->listar_por_slug('sobre');
        $texto = [];
        foreach ($pagina->textos as $v){
            $texto[] = [
                'nome' => $v->nome,
                'texto' => $v->texto
            ];
        }
        /*$colaboradores = [
            [
                'nome' => 'Lilian Leite - Criadora',
                'arquivo' => 'img/colaborador01.jpg',
                'descricao' => '',
            ],
            [
                'nome' => 'Dra. Letícia Gama - Veterinária',
                'arquivo' => 'img/colaborador03.jpg',
                'descricao' => '
                    Clínica Vet Gatos<br>
                    Rua Ulisses Sarmento, 135 A, Praia do Suá, Vitória - ES / Tel: (27) 3100-8180
                ',
            ],
            [
                'nome' => 'Dra. Fernanda Migliorine - Veterinária',
                'arquivo' => 'img/colaborador04.jpg',
                'descricao' => '',
            ],
            [
                'nome' => 'Dr. Ritialle Max Pastore - Veterinário',
                'arquivo' => 'img/colaborador05.jpg',
                'descricao' => 'CliVet <br> Rua Fortaleza, 1280, Itapuã, VilaVelha - ES / Tel: (27) 3062-1100',
            ],
            [
                'nome' => 'Premier Alimentos',
                'arquivo' => 'img/colaborador02.jpg',
                'descricao' => 'Vila Olímpia, em São Paulo<br><a href="http://www.premierpet.com.br/" target="_blank">www.premierpet.com.br</a>',
            ],
        ];
        $colaboradores = json_decode(json_encode($colaboradores));*/
        $colaboradores = $this->colaboradorRepository->listar();

        /*$depoimentos = [
            [
                'nome' => 'Gabriela Fazolo',
                'descricao' => 'Gostaria de deixar o meu depoimento com relação a vendedora Lilian e a compra do meu gato. Eu comprei ele pelo site da OLX, a Lilian foi muito atenciosa, respondeu todas as minhas perguntas e tbm foi flexível com relação a negociação do gatinho. Depois da compra ela continua me dando apoio pelo whatsapp, isso é muito bom. Ela é uma pessoa muito bacana e confiável, eu a recomendo de olhos fechados. Eu estou a quase 3 meses com o meu Olavo e eu sou muito apaixonada por ele, ele é super tranquilo, carinhoso e brincalhão ele é o melhor é mais lindo gato do mundo (rsrsrsr). Pessoal caso queriam comprar um gato com pedigree e com alguém de confiança, recomendo a Lilian.  Abraços.',
            ],
            [
                'nome' => 'Gatil Heros & Maiara Nova Friburgo, RJ',
                'descricao' => 'Lilian estou muito feliz com as gatinhas que comprei de vcs, são lindas... Muito bem cuidadas... Lilian é uma criadora muito atenciosa. Super indico.',
            ],
            [
                'nome' => 'Francyny, Vila Velha, ES',
                'descricao' => 'Recomendo o Gatil Neway, principalmente devido ao cuidado e carinho que a criadora Lilian Leite dedica não só às matrizes e aos padreadores, mas também aos filhotes nascidos no gatil. O ambiente é limpo e apropriado. A Lilian demonstra muito amor e respeito pelos animais. É sempre muito atenciosa, fazendo questão de esclarecer todas as dúvidas e auxiliar no que for preciso para uma boa adaptação dos bebês ao novo lar. Sempre prestativa, todas as vezes em que precisei de orientação ou dicas fui muito bem atendida.',
            ],
            [
                'nome' => '',
                'descricao' => '',
            ],
        ];
        $depoimentos = json_decode(json_encode($depoimentos));*/
        $depoimentos = $this->depoimentoRepository->ultimos();

        return view('site.sobre', compact('colaboradores','depoimentos', 'texto'));
    }

    public function artigos()
    {
        $artigos = $this->noticiaRepository->ultimos();
        $ids = [];
        foreach ($artigos as $v){
            $ids[] = $v->id;
        }
        $artigos2 = $this->noticiaRepository->outras($ids);

        return view('site.artigos',compact('artigos','artigos2'));
    }

    public function artigo($slug)
    {
        $artigo = $this->noticiaRepository->exibir_por_slug($slug);//Noticia::where(['slug' => $slug])->first();

        return view('site.artigo',compact('artigo'));
    }

    public function contato()
    {
        return view('site.contato');
    }

    public function contato_enviar(Request $request)
    {
        $valores = [
            'nome' => $request->input('nome'),
            'email' => $request->input('email'),
            'telefone' => $request->input('telefone'),
            'mensagem' => $request->input('mensagem'),
        ];

        Mail::send('site.contato_enviar', $valores, function ($message)
        {
            $message->subject('Contato Site');
            //$message->bcc('raphaellorencini@gmail.com');
            $message->cc('lilian.lp.santos@gmail.com');
        });

        $request->session()->flash('enviado', 'Mensagem enviada com sucesso!');

        return Redirect::to(URL::previous() . "#formulario");
    }

    public function acfe()
    {
        $pagina = $this->paginaRepository->listar_por_slug('acfe');
        $texto = [];
        foreach ($pagina->textos as $v){
            $texto[] = [
                'nome' => $v->nome,
                'texto' => $v->texto
            ];
        }

        return view('site.acfe',compact('texto'));
    }

    public function login()
    {
        return redirect('auth/login');
    }

    public function logout()
    {
        return redirect('auth/logout');
    }
    
    /**
     * Funções
     */
    private function fotos($grupo, $limit = 2)
    {
        $val_lista = $this->fotoCategoriaRepository->listar($grupo,'id', $limit);//FotoCategoria::where(['grupo' => $grupo])->limit($limit)->get();
        $val = [];
        $i = 0;
        foreach ($val_lista as $v){
            $val[$i] = [
                'id' => $v->id,
                'nome' => $v->nome,
                'arquivo' => $v->arquivo,
                'descricao' => $v->descricao,
            ];

            foreach ($v->fotos as $v2){
                $arquivo = $v2->arquivo;
                $classe_css = "";
                if(strpos($arquivo,'youtube') !== false){
                    $arquivo = \Youtube::embed($v2->arquivo);
                    $classe_css = "fancybox.iframe";
                }
                $val[$i]['fotos'][] = [
                    'foto_categoria_id' => $v2->foto_categoria_id,
                    'arquivo' => $arquivo,
                    'legenda' => $v2->legenda,
                    'classe_css' => $classe_css,
                ];
            }
            if(empty($val[$i]['fotos'])){
                $val[$i]['fotos'] = [];
            }
            $i++;
        }
        $val = json_decode(json_encode($val), false);

        return $val;
    }
}
