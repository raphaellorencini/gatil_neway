<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;

class AdminBannersController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::paginate();
        return view('admin.banners.index', compact('banners'));
    }

    public function novo()
    {
        return view('admin.banners.novo');
    }

    public function salvar(BannerRequest $request)
    {
        $data = $request->all();
        Banner::create($data);
        return redirect()->route('admin.banners.index');
    }

    public function editar($id)
    {
        $banner = Banner::find($id);
        return view('admin.banners.editar', compact('banner'));
    }

    public function atualizar(BannerRequest $request, $id)
    {
        $data = $request->all();
        Banner::find($id)->update($data);

        return redirect()->route('admin.banners.index');
    }
}
