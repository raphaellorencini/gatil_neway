<?php

namespace App\Http\Controllers\Admin;

use App\FotoCategoria;
use App\Http\Controllers\Controller;
use App\Http\Requests\FotoCategoriaRequest;
use App\Repositories\FotoCategoriaRepository;
use Illuminate\Http\Request;

class AdminFotosCategoriasController extends Controller
{
    /**
     * @var FotoCategoriaRepository
     */
    private $fotoCategoriaRepository;

    public function __construct(FotoCategoriaRepository $fotoCategoriaRepository)
    {
        $this->fotoCategoriaRepository = $fotoCategoriaRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $grupo = $request->get('grupo');
        if(empty($grupo)){
            $fotos_categorias = $this->fotoCategoriaRepository->listar_paginado([1,2,3,4]);
        }else{
            $fotos_categorias = $this->fotoCategoriaRepository->listar_paginado($grupo);
        }

        return view('admin.fotos_categorias.index', compact('fotos_categorias','grupo'));
    }

    public function novo()
    {
        return view('admin.fotos_categorias.novo');
    }

    public function salvar(FotoCategoriaRequest $request)
    {
        $data = $request->all();
        FotoCategoria::create($data);
        return redirect()->route('admin.fotos_categorias.index');
    }

    public function editar($id)
    {
        $foto_categoria = FotoCategoria::find($id);
        return view('admin.fotos_categorias.editar', compact('foto_categoria'));
    }

    public function atualizar(FotoCategoriaRequest $request, $id)
    {
        $data = $request->all();
        FotoCategoria::find($id)->update($data);

        return redirect()->route('admin.fotos_categorias.index');
    }

    public function delete($id)
    {
        FotoCategoria::find($id)->delete();

        return redirect()->route('admin.fotos_categorias.index');
    }
}
