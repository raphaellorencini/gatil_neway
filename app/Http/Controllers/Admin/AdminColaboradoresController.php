<?php

namespace App\Http\Controllers\Admin;

use App\Colaborador;
use App\Http\Controllers\Controller;
use App\Http\Requests\ColaboradorRequest;
use App\Repositories\ColaboradorRepository;


class AdminColaboradoresController extends Controller
{

    /**
     * @var ColaboradorRepository
     */
    private $colaboradorRepository;

    public function __construct(ColaboradorRepository $colaboradorRepository)
    {
        $this->colaboradorRepository = $colaboradorRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colaboradores = $this->colaboradorRepository->listar();
        return view('admin.colaboradores.index', compact('colaboradores'));
    }

    public function novo()
    {
        return view('admin.colaboradores.novo');
    }

    public function salvar(ColaboradorRequest $request)
    {

        $data = $request->all();
        Colaborador::create($data);
        return redirect()->route('admin.colaboradores.index');
    }

    public function editar($id)
    {
        $colaborador = Colaborador::find($id);
        return view('admin.colaboradores.editar', compact('colaborador'));
    }

    public function atualizar(ColaboradorRequest $request, $id)
    {
        $data = $request->all();
        Colaborador::find($id)->update($data);

        return redirect()->route('admin.colaboradores.index');
    }

    public function delete($id)
    {
        Colaborador::find($id)->delete();

        return redirect()->route('admin.colaboradores.index');
    }
}
