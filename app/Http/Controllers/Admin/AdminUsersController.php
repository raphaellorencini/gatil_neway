<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserEditPasswordRequest;
use App\Http\Requests\UserEditRequest;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;


class AdminUsersController extends Controller
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userRepository->listar();
        return view('admin.users.index', compact('users'));
    }

    public function novo()
    {
        return view('admin.users.novo');
    }

    public function salvar(UserRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = str_random(60);
        User::create($data);
        return redirect()->route('admin.users.index');
    }

    public function editar($id)
    {
        $user = User::find($id);
        return view('admin.users.editar', compact('user'));
    }

    public function atualizar(UserEditRequest $request, $id)
    {
        $data = $request->all();
        unset($data['email']);
        User::find($id)->update($data);

        return redirect()->route('admin.users.index');
    }

    public function senha_editar($id)
    {
        $user = User::find($id);
        return view('admin.users.senha_editar', compact('user'));
    }

    public function senha_atualizar(UserEditPasswordRequest $request, $id)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = str_random(60);
        User::find($id)->update($data);

        return redirect()->route('admin.users.index');
    }

    public function delete($id)
    {
        User::find($id)->delete();

        return redirect()->route('admin.users.index');
    }
}
