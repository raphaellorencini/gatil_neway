<?php

namespace App\Http\Controllers\Admin;

use App\PaginaTexto;
use App\Pagina;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaginaTextoRequest;

class AdminPaginasTextosController extends Controller
{

    public function index($pagina_id)
    {
        $pagina  = Pagina::find($pagina_id);
        $paginas_textos = PaginaTexto::where([
            'pagina_id' => $pagina_id
        ])->paginate();

        return view('admin.paginas_textos.index', compact('paginas_textos','pagina','pagina_id'));
    }

    public function novo($pagina_id)
    {
        $pagina  = Pagina::find($pagina_id);
        return view('admin.paginas_textos.novo',compact('pagina','pagina_id'));
    }

    public function salvar(PaginaTextoRequest $request, $pagina_id)
    {
        $data = $request->all();
        $data['pagina_id'] = $pagina_id;

        PaginaTexto::create($data);

        return redirect()->route('admin.paginas_textos.index',['pagina_id' => $pagina_id]);
    }

    public function editar($id)
    {
        $pagina_texto = PaginaTexto::find($id);
        $pagina_id = $pagina_texto->pagina_id;
        $pagina  = Pagina::find($pagina_id);
        return view('admin.paginas_textos.editar', compact('pagina_texto','pagina','pagina_id'));
    }

    public function atualizar(PaginaTextoRequest $request, $id)
    {
        $data = $request->all();
        $pagina_texto = PaginaTexto::find($id);
        $pagina_texto->update($data);
        $pagina_id = $pagina_texto->pagina_id;

        return redirect()->route('admin.paginas_textos.index',['pagina_id' => $pagina_id]);
    }

    public function delete($id)
    {
        $pagina_texto = PaginaTexto::find($id);
        $pagina_id = $pagina_texto->pagina_id;
        $pagina_texto->delete();

        return redirect()->route('admin.paginas_textos.index',['pagina_id' => $pagina_id]);
    }

}
