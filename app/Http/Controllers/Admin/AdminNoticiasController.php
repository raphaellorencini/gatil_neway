<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NoticiaRequest;
use App\Noticia;
use App\Http\Controllers\Controller;

class AdminNoticiasController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artigos = Noticia::orderBy('created_at','DESC')->paginate();
        return view('admin.artigos.index', compact('artigos'));
    }

    public function novo()
    {
        return view('admin.artigos.novo');
    }

    public function salvar(NoticiaRequest $request)
    {
        $data = $request->all();
        Noticia::create($data);
        return redirect()->route('admin.artigos.index');
    }

    public function editar($id)
    {
        $artigo = Noticia::find($id);
        return view('admin.artigos.editar', compact('artigo'));
    }

    public function atualizar(NoticiaRequest $request, $id)
    {
        $data = $request->all();
        Noticia::find($id)->update($data);

        return redirect()->route('admin.artigos.index');
    }

    public function delete($id)
    {
        Noticia::find($id)->delete();

        return redirect()->route('admin.artigos.index');
    }
}
