<?php

namespace App\Http\Controllers\Admin;

use App\Foto;
use App\FotoCategoria;
use App\Http\Controllers\Controller;
use App\Http\Requests\FotoRequest;
use App\Repositories\FotoRepository;

class AdminFotosController extends Controller
{
    /**
     * @var FotoRepository
     */
    private $fotoRepository;

    public function __construct(FotoRepository $fotoRepository)
    {
        $this->fotoRepository = $fotoRepository;
    }

    public function index($foto_categoria_id)
    {
        $foto_categoria  = FotoCategoria::find($foto_categoria_id);
        $fotos = $this->fotoRepository->listar($foto_categoria_id);

        $botoes = $this->botoes($foto_categoria_id);

        return view('admin.fotos.index', compact('fotos','foto_categoria','foto_categoria_id', 'botoes'));
    }

    public function novo($foto_categoria_id)
    {
        $foto_categoria  = FotoCategoria::find($foto_categoria_id);

        $botoes = $this->botoes($foto_categoria_id);

        return view('admin.fotos.novo',compact('foto_categoria','foto_categoria_id','botoes'));
    }

    public function salvar(FotoRequest $request, $foto_categoria_id)
    {
        $fotos = json_decode($request->get('fotos'), false);

        foreach ($fotos as $v){
            if(!empty($v->arquivo)){
                $foto = new Foto();
                $foto->foto_categoria_id = $v->foto_categoria_id;
                $foto->arquivo = $v->arquivo;
                $foto->legenda = !empty($v->legenda) ? $v->legenda : "";
                $foto->save();
            }
        }

        return redirect()->route('admin.fotos.index',['foto_categoria_id' => $foto_categoria_id]);
    }

    public function novo2($foto_categoria_id)
    {
        $foto_categoria  = FotoCategoria::find($foto_categoria_id);

        $botoes = $this->botoes($foto_categoria_id);

        return view('admin.fotos.novo2',compact('foto_categoria','foto_categoria_id','botoes'));
    }

    public function salvar2(FotoRequest $request, $id)
    {
        $data = $request->all();
        $data['foto_categoria_id'] = $id;
        Foto::create($data);

        return redirect()->route('admin.fotos.index',['foto_categoria_id' => $id]);
    }

    public function editar($id)
    {
        $foto = Foto::find($id);
        $foto_categoria_id = $foto->foto_categoria_id;
        $foto_categoria  = FotoCategoria::find($foto_categoria_id);
        $botoes = $this->botoes($foto_categoria_id);
        return view('admin.fotos.editar', compact('foto','foto_categoria','foto_categoria_id','botoes'));
    }

    public function atualizar(FotoRequest $request, $id)
    {
        $data = $request->all();
        $foto = Foto::find($id);
        $foto->update($data);
        $foto_categoria_id = $foto->foto_categoria_id;

        return redirect()->route('admin.fotos.index',['foto_categoria_id' => $foto_categoria_id]);
    }

    public function delete($id)
    {
        $foto = Foto::find($id);
        $foto_categoria_id = $foto->foto_categoria_id;
        $foto->delete();

        return redirect()->route('admin.fotos.index',['foto_categoria_id' => $foto_categoria_id]);
    }

    private function botoes($id)
    {
        $val = [];
        if($id != 12){
            $val['titulo'] = 'Fotos - ';
            $val['titulo2'] = 'Imagem';
            $val['novo'] = 'Novas Fotos';
            $val['voltar'] = 'Voltar para Categorias';
        }else{
            $val['titulo'] = '';
            $val['titulo2'] = 'Vídeo';
            $val['novo'] = 'Novos Vídeos';
            $val['voltar'] = '';
        }

        return $val;
    }

}
