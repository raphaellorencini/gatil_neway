<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



/*Site*/
Route::get('/',['as' => 'index', 'uses' => 'SiteController@index']);

Route::group(['as' => 'site.'], function(){
    Route::get('index',['as' => 'index', 'uses' => 'SiteController@index']);
    Route::get('sobre',['as' => 'sobre', 'uses' => 'SiteController@sobre']);
    Route::get('artigos',['as' => 'artigos', 'uses' => 'SiteController@artigos']);
    Route::get('artigo/{slug}',['as' => 'artigo', 'uses' => 'SiteController@artigo']);
    Route::get('contato',['as' => 'contato', 'uses' => 'SiteController@contato']);
    Route::post('contato_enviar',['as' => 'contato_enviar', 'uses' => 'SiteController@contato_enviar']);
    Route::get('nossos_gatos',['as' => 'nossos_gatos', 'uses' => 'SiteController@nossos_gatos']);
    Route::get('disponiveis',['as' => 'disponiveis', 'uses' => 'SiteController@disponiveis']);
    Route::get('videos',['as' => 'videos', 'uses' => 'SiteController@videos']);
    Route::get('acfe',['as' => 'acfe', 'uses' => 'SiteController@acfe']);

    Route::get('login',['as' => 'login', 'uses' => 'SiteController@login']);
    Route::get('logout',['as' => 'logout', 'uses' => 'SiteController@logout']);

    Route::get('teste',['as' => 'teste', 'uses' => 'SiteController@teste']);
});

/*Admin*/
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['web','auth']], function(){
    Route::get('/',['as' => 'index', 'uses' => 'AdminHomeController@index']);
    Route::get('home',['as' => 'home', 'uses' => 'AdminHomeController@index']);

    Route::get('banners',['as' => 'banners.index', 'uses' => 'AdminBannersController@index']);
    Route::get('banners/novo',['as' => 'banners.novo', 'uses' => 'AdminBannersController@novo']);
    Route::get('banners/editar/{id}',['as' => 'banners.editar', 'uses' => 'AdminBannersController@editar']);
    Route::post('banners/salvar',['as' => 'banners.salvar', 'uses' => 'AdminBannersController@salvar']);
    Route::post('banners/atualizar/{id}',['as' => 'banners.atualizar', 'uses' => 'AdminBannersController@atualizar']);

    Route::get('fotos_categorias',['as' => 'fotos_categorias.index', 'uses' => 'AdminFotosCategoriasController@index']);
    Route::get('fotos_categorias/novo',['as' => 'fotos_categorias.novo', 'uses' => 'AdminFotosCategoriasController@novo']);
    Route::get('fotos_categorias/editar/{id}',['as' => 'fotos_categorias.editar', 'uses' => 'AdminFotosCategoriasController@editar']);
    Route::post('fotos_categorias/salvar',['as' => 'fotos_categorias.salvar', 'uses' => 'AdminFotosCategoriasController@salvar']);
    Route::post('fotos_categorias/atualizar/{id}',['as' => 'fotos_categorias.atualizar', 'uses' => 'AdminFotosCategoriasController@atualizar']);
    Route::get('fotos_categorias/delete/{id}',['as' => 'fotos_categorias.delete', 'uses' => 'AdminFotosCategoriasController@delete']);

    Route::get('fotos/{foto_categoria_id}',['as' => 'fotos.index', 'uses' => 'AdminFotosController@index']);
    Route::get('fotos/novo/{foto_categoria_id}',['as' => 'fotos.novo', 'uses' => 'AdminFotosController@novo']);
    Route::get('fotos/novo2/{foto_categoria_id}',['as' => 'fotos.novo2', 'uses' => 'AdminFotosController@novo2']);
    Route::get('fotos/editar/{id}',['as' => 'fotos.editar', 'uses' => 'AdminFotosController@editar']);
    Route::post('fotos/salvar/{foto_categoria_id}',['as' => 'fotos.salvar', 'uses' => 'AdminFotosController@salvar']);
    Route::post('fotos/salvar2/{foto_categoria_id}',['as' => 'fotos.salvar2', 'uses' => 'AdminFotosController@salvar2']);
    Route::post('fotos/atualizar/{id}',['as' => 'fotos.atualizar', 'uses' => 'AdminFotosController@atualizar']);
    Route::get('fotos/delete/{id}',['as' => 'fotos.delete', 'uses' => 'AdminFotosController@delete']);

    Route::get('artigos',['as' => 'artigos.index', 'uses' => 'AdminNoticiasController@index']);
    Route::get('artigos/novo',['as' => 'artigos.novo', 'uses' => 'AdminNoticiasController@novo']);
    Route::get('artigos/editar/{id}',['as' => 'artigos.editar', 'uses' => 'AdminNoticiasController@editar']);
    Route::post('artigos/salvar',['as' => 'artigos.salvar', 'uses' => 'AdminNoticiasController@salvar']);
    Route::post('artigos/atualizar/{id}',['as' => 'artigos.atualizar', 'uses' => 'AdminNoticiasController@atualizar']);
    Route::get('artigos/delete/{id}',['as' => 'artigos.delete', 'uses' => 'AdminNoticiasController@delete']);

    Route::get('paginas',['as' => 'paginas.index', 'uses' => 'AdminPaginasController@index']);
    Route::get('paginas/novo',['as' => 'paginas.novo', 'uses' => 'AdminPaginasController@novo']);
    Route::get('paginas/editar/{id}',['as' => 'paginas.editar', 'uses' => 'AdminPaginasController@editar']);
    Route::post('paginas/salvar',['as' => 'paginas.salvar', 'uses' => 'AdminPaginasController@salvar']);
    Route::post('paginas/atualizar/{id}',['as' => 'paginas.atualizar', 'uses' => 'AdminPaginasController@atualizar']);
    Route::get('paginas/delete/{id}',['as' => 'paginas.delete', 'uses' => 'AdminPaginasController@delete']);

    Route::get('paginas_textos/{pagina_id}',['as' => 'paginas_textos.index', 'uses' => 'AdminPaginasTextosController@index']);
    Route::get('paginas_textos/novo/{pagina_id}',['as' => 'paginas_textos.novo', 'uses' => 'AdminPaginasTextosController@novo']);
    Route::get('paginas_textos/editar/{id}',['as' => 'paginas_textos.editar', 'uses' => 'AdminPaginasTextosController@editar']);
    Route::post('paginas_textos/salvar/{pagina_id}',['as' => 'paginas_textos.salvar', 'uses' => 'AdminPaginasTextosController@salvar']);
    Route::post('paginas_textos/atualizar/{id}',['as' => 'paginas_textos.atualizar', 'uses' => 'AdminPaginasTextosController@atualizar']);
    Route::get('paginas_textos/delete/{id}',['as' => 'paginas_textos.delete', 'uses' => 'AdminPaginasTextosController@delete']);

    Route::get('colaboradores',['as' => 'colaboradores.index', 'uses' => 'AdminColaboradoresController@index']);
    Route::get('colaboradores/novo',['as' => 'colaboradores.novo', 'uses' => 'AdminColaboradoresController@novo']);
    Route::get('colaboradores/editar/{id}',['as' => 'colaboradores.editar', 'uses' => 'AdminColaboradoresController@editar']);
    Route::post('colaboradores/salvar',['as' => 'colaboradores.salvar', 'uses' => 'AdminColaboradoresController@salvar']);
    Route::post('colaboradores/atualizar/{id}',['as' => 'colaboradores.atualizar', 'uses' => 'AdminColaboradoresController@atualizar']);
    Route::get('colaboradores/delete/{id}',['as' => 'colaboradores.delete', 'uses' => 'AdminColaboradoresController@delete']);

    Route::get('depoimentos',['as' => 'depoimentos.index', 'uses' => 'AdminDepoimentosController@index']);
    Route::get('depoimentos/novo',['as' => 'depoimentos.novo', 'uses' => 'AdminDepoimentosController@novo']);
    Route::get('depoimentos/editar/{id}',['as' => 'depoimentos.editar', 'uses' => 'AdminDepoimentosController@editar']);
    Route::post('depoimentos/salvar',['as' => 'depoimentos.salvar', 'uses' => 'AdminDepoimentosController@salvar']);
    Route::post('depoimentos/atualizar/{id}',['as' => 'depoimentos.atualizar', 'uses' => 'AdminDepoimentosController@atualizar']);
    Route::get('depoimentos/delete/{id}',['as' => 'depoimentos.delete', 'uses' => 'AdminDepoimentosController@delete']);

    Route::get('users',['as' => 'users.index', 'uses' => 'AdminUsersController@index']);
    Route::get('users/novo',['as' => 'users.novo', 'uses' => 'AdminUsersController@novo']);
    Route::get('users/editar/{id}',['as' => 'users.editar', 'uses' => 'AdminUsersController@editar']);
    Route::post('users/salvar',['as' => 'users.salvar', 'uses' => 'AdminUsersController@salvar']);
    Route::post('users/atualizar/{id}',['as' => 'users.atualizar', 'uses' => 'AdminUsersController@atualizar']);
    Route::get('users/senha_editar/{id}',['as' => 'users.senha_editar', 'uses' => 'AdminUsersController@senha_editar']);
    Route::post('users/senha_atualizar/{id}',['as' => 'users.senha_atualizar', 'uses' => 'AdminUsersController@senha_atualizar']);
    Route::get('users/delete/{id}',['as' => 'users.delete', 'uses' => 'AdminUsersController@delete']);
});

/*Auth*/
Route::group(['prefix' => 'auth', 'as' => 'auth.','middleware' => ['web']], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
});

/*Exibe as SQL's executadas*/
#Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {var_dump($query->sql);var_dump($query->bindings);var_dump($query->time);});