<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublicadoToFotosCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fotos_categorias', function (Blueprint $table) {
            $table->enum('publicado',['1','2'])->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('fotos_categorias', function (Blueprint $table) {
            $table->dropColumn('publicado');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
