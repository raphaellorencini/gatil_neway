<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaginasTextosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paginas_textos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pagina_id')->unsigned();
            $table->foreign('pagina_id')
                ->references('id')
                ->on('paginas')
                ->onDelete('cascade');

            $table->string('nome');
            $table->text('texto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('paginas_textos');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
