<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foto_categoria_id')->unsigned();
            $table->foreign('foto_categoria_id')
                ->references('id')
                ->on('fotos_categorias')
                ->onDelete('cascade');

            $table->string('legenda')->nullable();
            $table->text('arquivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('fotos');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
