<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(FotosCategoriasTableSeeder::class);
        $this->call(FotosTableSeeder::class);
        $this->call(NoticiasTableSeeder::class);
        $this->call(PaginasTableSeeder::class);
        $this->call(PaginasTextosTableSeeder::class);

        Model::reguard();
    }
}
