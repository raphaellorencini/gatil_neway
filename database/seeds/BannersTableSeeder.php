<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/banner01.jpg',
            'legenda' => 'Encontre seu melhor amigo e grande companheiro!',
        ]);

        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/banner02.jpg',
            'legenda' => 'Viva uma belíssima experiência de amor, gratidão e fidelidade!',
        ]);

        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/banner03.jpg',
            'legenda' => 'Gatos te ensinam a parar e ver as belezas do mundo!',
        ]);

        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/banner04.jpg',
            'legenda' => 'Encontre seu melhor amigo e grande companheiro!',
        ]);

        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/banner05.jpg',
            'legenda' => 'Viva uma belíssima experiência de amor, gratidão e fidelidade!',
        ]);

        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/banner06.jpg',
            'legenda' => 'Gatos te ensinam a parar e ver as belezas do mundo!',
        ]);
    }
}
