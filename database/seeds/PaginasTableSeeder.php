<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            'nome' => 'Home',
            'slug' => 'home',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas')->insert([
            'nome' => 'Sobre',
            'slug' => 'sobre',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas')->insert([
            'nome' => 'Disponíveis',
            'slug' => 'disponiveis',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas')->insert([
            'nome' => 'ACFe',
            'slug' => 'acfe',
            'created_at' => new DateTime(),
        ]);
    }
}
