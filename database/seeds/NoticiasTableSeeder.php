<?php

use Illuminate\Database\Seeder;

class NoticiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('noticias')->insert([
            'titulo' => 'Class aptent taciti sociosqu ad litora torquent per conubia nostra',
            'slug' => 'class-aptent-taciti-sociosqu-ad-litora-torquent-per-conubia-nostra',
            'arquivo' => '/uploads/arquivos/gatil_neway_066.jpg',
            'publicado' => '1',
            'texto' => 'Nulla gravida, quam eu ultricies consectetur, eros risus aliquam nulla, quis rhoncus magna elit nec sem. In augue tortor, efficitur quis neque in, aliquet imperdiet justo. Praesent et orci ac risus porta tristique. Integer id lorem iaculis, aliquet mi ut, pharetra tellus. Nunc id felis non neque rhoncus gravida in in mi. Aliquam erat volutpat. Pellentesque vehicula vehicula orci, et facilisis ligula hendrerit euismod. Donec convallis ultricies consectetur. Maecenas nibh nulla, aliquam ac est at, hendrerit eleifend magna. Nullam nunc nisi, sodales vel dapibus vitae, molestie suscipit erat.',
            'created_at' => '2016-09-24 10:00:00',
        ]);

        DB::table('noticias')->insert([
            'titulo' => 'Donec nec massa et mauris rhoncus ornare',
            'slug' => 'donec-nec-massa-et-mauris-rhoncus-ornare',
            'arquivo' => '/uploads/arquivos/gatil_neway_027.jpg',
            'publicado' => '1',
            'texto' => 'Integer finibus lectus vitae ultricies porttitor. Suspendisse tincidunt erat lorem, vitae vulputate magna sodales at. Aenean quis lorem vel quam accumsan suscipit non eu dolor. Sed dapibus aliquam velit non cursus. Curabitur rutrum in lectus volutpat efficitur. Etiam tincidunt purus et efficitur egestas. Maecenas eget cursus dui, quis pellentesque nisi. Pellentesque erat augue, pulvinar quis enim ac, congue mattis massa. Pellentesque eget posuere quam. Phasellus luctus sollicitudin erat, in varius mi. Curabitur ac congue ante.',
            'created_at' => '2016-09-25 13:30:00',
        ]);

        DB::table('noticias')->insert([
            'titulo' => 'Aliquam vulputate placerat ipsum',
            'slug' => 'aliquam-vulputate-placerat-ipsum',
            'arquivo' => '/uploads/arquivos/gatil_neway_036.jpg',
            'publicado' => '1',
            'texto' => 'Suspendisse posuere elit nec hendrerit ornare. Aenean sagittis ante sed massa gravida, eu pellentesque erat consequat. Quisque lacus tortor, ornare in purus non, euismod consectetur nunc. Phasellus auctor lacus sit amet nisi pulvinar, eu porttitor ex ornare. Sed sollicitudin dignissim lectus, vitae tincidunt lacus ultricies et. Sed vel ullamcorper sem. Cras ullamcorper scelerisque est vel tempor. Donec at egestas ligula, et tristique risus. Sed aliquam dapibus diam non feugiat. Etiam non arcu diam.',
            'created_at' => '2016-09-26 15:10:00',
        ]);

        DB::table('noticias')->insert([
            'titulo' => 'Xyz Class aptent taciti sociosqu ad litora torquent per conubia nostra',
            'slug' => 'xyz-class-aptent-taciti-sociosqu-ad-litora-torquent-per-conubia-nostra',
            'arquivo' => '/uploads/arquivos/gatil_neway_066.jpg',
            'publicado' => '1',
            'texto' => 'Nulla gravida, quam eu ultricies consectetur, eros risus aliquam nulla, quis rhoncus magna elit nec sem. In augue tortor, efficitur quis neque in, aliquet imperdiet justo. Praesent et orci ac risus porta tristique. Integer id lorem iaculis, aliquet mi ut, pharetra tellus. Nunc id felis non neque rhoncus gravida in in mi. Aliquam erat volutpat. Pellentesque vehicula vehicula orci, et facilisis ligula hendrerit euismod. Donec convallis ultricies consectetur. Maecenas nibh nulla, aliquam ac est at, hendrerit eleifend magna. Nullam nunc nisi, sodales vel dapibus vitae, molestie suscipit erat.',
            'created_at' => '2016-09-27 10:00:00',
        ]);

        DB::table('noticias')->insert([
            'titulo' => 'Abc Donec nec massa et mauris rhoncus ornare',
            'slug' => 'abc-donec-nec-massa-et-mauris-rhoncus-ornare',
            'arquivo' => '/uploads/arquivos/gatil_neway_027.jpg',
            'publicado' => '1',
            'texto' => 'Integer finibus lectus vitae ultricies porttitor. Suspendisse tincidunt erat lorem, vitae vulputate magna sodales at. Aenean quis lorem vel quam accumsan suscipit non eu dolor. Sed dapibus aliquam velit non cursus. Curabitur rutrum in lectus volutpat efficitur. Etiam tincidunt purus et efficitur egestas. Maecenas eget cursus dui, quis pellentesque nisi. Pellentesque erat augue, pulvinar quis enim ac, congue mattis massa. Pellentesque eget posuere quam. Phasellus luctus sollicitudin erat, in varius mi. Curabitur ac congue ante.',
            'created_at' => '2016-09-28 13:30:00',
        ]);

        DB::table('noticias')->insert([
            'titulo' => 'Zaq Aliquam vulputate placerat ipsum',
            'slug' => 'zaq-aliquam-vulputate-placerat-ipsum',
            'arquivo' => '/uploads/arquivos/gatil_neway_036.jpg',
            'publicado' => '1',
            'texto' => 'Suspendisse posuere elit nec hendrerit ornare. Aenean sagittis ante sed massa gravida, eu pellentesque erat consequat. Quisque lacus tortor, ornare in purus non, euismod consectetur nunc. Phasellus auctor lacus sit amet nisi pulvinar, eu porttitor ex ornare. Sed sollicitudin dignissim lectus, vitae tincidunt lacus ultricies et. Sed vel ullamcorper sem. Cras ullamcorper scelerisque est vel tempor. Donec at egestas ligula, et tristique risus. Sed aliquam dapibus diam non feugiat. Etiam non arcu diam.',
            'created_at' => '2016-09-29 15:10:00',
        ]);
    }
}
