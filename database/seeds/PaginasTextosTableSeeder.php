<?php

use Illuminate\Database\Seeder;

class PaginasTextosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas_textos')->insert([
            'pagina_id' => '1',
            'nome' => 'Saiba tudo sobre seu gato!',
            'texto' => '<p>Os gatos s&atilde;o excelentes companheiros, com a vantagem adicional de n&atilde;o precisarem de aten&ccedil;&atilde;o integral. O gato se contenta em ficar ao seu lado, mesmo enquanto voc&ecirc; est&aacute; ocupado com outras coisas. Eles n&atilde;o necessitam de grandes espa&ccedil;os para viver, vivendo tranquilamente em um apartamento.</p>

<p>Ao contr&aacute;rio dos c&atilde;es, os gatos n&atilde;o se deixam &ldquo;possuir&rdquo; com facilidade. O afeto do gato precisa ser conquistado. Ele n&atilde;o tolerar&aacute; um mau dono, em fun&ccedil;&atilde;o de um senso de lealdade equivocado, mas reconhecer&aacute; um dono atencioso e compreensivo, retribuindo com amizade e respeito. &Eacute; preciso entender sua natureza e trat&aacute;-lo adequadamente.</p>
',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '1',
            'nome' => 'Nossos Serviços',
            'texto' => '<p>Antes de o gatinho nascer, existe uma sele&ccedil;&atilde;o gen&eacute;tica.</p>

<p>Uma vez escolhido o casal, come&ccedil;a um acompanhamento constante da gesta&ccedil;&atilde;o, a gata deve estar com sa&uacute;de perfeita e peso adequado, o parto e assistido, bem como todo o desenvolvimento da ninhada; cada gatinho e avaliado quanto ao seu desenvolvimento, pesado vermifugado, alimentado de forma complementar, vitaminado, e vacinado na idade correta.</p>

<p>N&atilde;o os disponibilizamos antes de tr&ecirc;s meses de vida, por acreditarmos que a socializa&ccedil;&atilde;o com seus irm&atilde;ozinhos ser muito importante para se tornar um gato esperto e carinhoso.</p>

<p>Na idade certa para machos e f&ecirc;meas, ser&aacute; castrado assegurando assim, melhor sa&uacute;de futura e bem estar para ele e sua nova fam&iacute;lia.</p>

<p>Todo esse processo &eacute; feito com muito amor e aten&ccedil;&atilde;o.</p>
',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '2',
            'nome' => 'Seja Bem-Vindo ao nosso Site!',
            'texto' => '<h4>Palavra da Criadora</h4>

<p>&quot;Os Gatos sempre foram para mim, um complemento de minha vida, hoje n&atilde;o consigo me ver sem a companhia deles.</p>

<p>Queria amiguinhos eternos; ent&atilde;o pensei, como?</p>

<p>Comecei a pesquisar sobre ra&ccedil;as de gatos e encontrei o gato Persa como o mais indicado para quem quer ter seu gato por perto, sem medo de fuga, pois eles s&atilde;o mais tranquilos, caseiros que outras ra&ccedil;as.</p>

<p>Para beneficio dos fofuxos; uma boa alimenta&ccedil;&atilde;o, um local tranquilo e seguro, (redes) nas janelas e piscinas, vacina&ccedil;&atilde;o na &eacute;poca certa, consultas peri&oacute;dicas ao veterin&aacute;rio, exames preventivos das principais doen&ccedil;as e alem de tudo desparasitar, tanto eles como o ambiente onde moram.</p>

<p>Hoje existe relatos de gatos muito bem cuidados que viveram at&eacute; 20 anos, isso indica que podemos colaborar para que tenham uma vida longa</p>

<p>Foi ent&atilde;o que em junho de 2006, registramos nosso Gatil na CFB e come&ccedil;amos a criar esta ra&ccedil;a apaixonante &quot;OS PERSAS&quot;, e procuramos criar filhotes de qualidade gen&eacute;tica, saud&aacute;veis e de temperamento afetuoso, que por toda vida seja seu motivo de alegria, companheirismo e crescimento pessoal.</p>

<p>Quando voc&ecirc; adquiri um gatinho de um Gatil com padr&atilde;o de qualidade tem a certeza que houve toda uma operacionaliza&ccedil;&atilde;o para que esse gatinho viesse a exist&ecirc;ncia, o que condiz com o valor pago por ele.</p>

<p>Procuramos manter o custo operacional de nosso Gati num patamar m&eacute;dio para facilitar a aquisi&ccedil;&atilde;o, e manter o padr&atilde;o de qualidade do Gatil.</p>

<p>Agora que voc&ecirc; conhece nossa verdadeira inten&ccedil;&atilde;o, pedimos que se delicie na p&aacute;gina de filhotes dispon&iacute;veis, se por acaso n&atilde;o tiver nenhum dispon&iacute;vel, nos mande um email e mostre seu desejo, guardaremos com carinho seu email e logo que tivermos seu gatinho entraremos em contato, mas se voc&ecirc; encontrar seu amiguinho entre em contato comigo.&quot;</p>

<h5><em>Lilian Leite</em></h5>
',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '3',
            'nome' => 'Texto',
            'texto' => '<p>Nosso Gatil est&aacute; situado na cidade de Vit&oacute;ria, ES; registrado sob o n&ordm; 247/2006 na CFB (Confedera&ccedil;&atilde;o de Felinos do Brasil), filiado &agrave; WCF (World Cat Federation).</p>

<p>Despachamos para todo o Brasil. Todos os filhotes saem do Gatil vermifugados e com carteira de vacina&ccedil;&atilde;o atualizada, com direito a Pedigree, e contrato de castra&ccedil;&atilde;o.</p>

<p>Todos s&atilde;o alimentados com ra&ccedil;&atilde;o Premier.<br />
<a href="http://www.premierpet.com.br" style="display: inline-block;" target="_blank"><img alt="" src="/img/premier.png" /></a></p>

<p>S&atilde;o Criados livres em ambiente familiar.</p>

<p>Antes de ver os beb&ecirc;s dispon&iacute;veis, assista o lindo v&iacute;deo do RonRon do gatinho!</p>

<div style="text-align: center"><iframe allowfullscreen="" frameborder="0" height="300" src="https://www.youtube.com/embed/ZSXtRDSPmKs" width="500"></iframe></div>
',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '4',
            'nome' => 'SOBRE A ACFe',
            'texto' => '<p>Em 2007, se viu a necessidade de termos uma representa&ccedil;&atilde;o da CFB - Confedera&ccedil;&atilde;o de Felinos do Brasil no estado do ES.</p>

<p>A presidente da CFB, Sra Sylvia Roriz entrou em contato comigo e me incentivou a criar uma representa&ccedil;&atilde;o e foi ent&atilde;o que com muita alegria foi fundada a ACFe - Associa&ccedil;&atilde;o Capixaba de Felinos, da qual sou presidente desde ent&atilde;o.</p>

<p>Para mim &eacute; uma grande honra de ter sido escolhida para ser a fundadora e presidente da ACFe.</p>

<p><br />
<a href="http://asscapixabadefelinos.blogspot.com.br" target="_blank"><img alt="" src="/img/acfe.png" /></a></p>
',
            'created_at' => new DateTime(),
        ]);
    }
}
