<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com.br',
            'password' => bcrypt('admin@zaq'),
            'remember_token' => str_random(10),
        ]);

        factory(User::class)->create([
            'name' => 'Lilian',
            'email' => 'lilian@gatilneway.com.br',
            'password' => bcrypt('lilian@zaq'),
            'remember_token' => str_random(10),
        ]);
    }
}
