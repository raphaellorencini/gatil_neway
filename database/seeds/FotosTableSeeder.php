<?php

use Illuminate\Database\Seeder;

class FotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fotos')->insert([
            'foto_categoria_id' => '1',
            'arquivo' => '/uploads/arquivos/gatil_neway_072.jpg',
            'legenda' => 'Proin iaculis orci eget quam vestibulum.',
        ]);
        DB::table('fotos')->insert([
            'foto_categoria_id' => '1',
            'arquivo' => '/uploads/arquivos/gatil_neway_073.jpg',
            'legenda' => 'Integer malesuada enim lacus.',
        ]);

        DB::table('fotos')->insert([
            'foto_categoria_id' => '2',
            'arquivo' => '/uploads/arquivos/gatil_neway_069.jpg',
            'legenda' => 'Curabitur eget ex cursus, placerat nisl vitae.',
        ]);

        DB::table('fotos')->insert([
            'foto_categoria_id' => '2',
            'arquivo' => '/uploads/arquivos/gatil_neway_077.jpg',
            'legenda' => 'Etiam malesuada blandit nisi.',
        ]);

        DB::table('fotos')->insert([
            'foto_categoria_id' => '3',
            'arquivo' => '/uploads/arquivos/gatil_neway_083.jpg',
            'legenda' => 'Phasellus sagittis quam ac consectetur feugiat.',
        ]);

        DB::table('fotos')->insert([
            'foto_categoria_id' => '4',
            'arquivo' => '/uploads/arquivos/gatil_neway_079.jpg',
            'legenda' => 'Donec sit amet malesuada quam.',
        ]);

        DB::table('fotos')->insert([
            'foto_categoria_id' => '4',
            'arquivo' => '/uploads/arquivos/gatil_neway_081.jpg',
            'legenda' => 'Vestibulum a dolor ut nibh efficitur facilisis.',
        ]);
    }
}
