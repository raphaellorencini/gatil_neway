<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85305439-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-85305439-1');
    </script>
    <title>Gatil Neway - Gatos Persas, Himalaias e Exóticos</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-language" content="pt-br">
    <link rel="icon" href="{{URL::asset('img/favicon.ico')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{URL::asset('img/favicon.ico')}}" type="image/x-icon" />
    <meta name="keywords" content="gatil, gato, gatil neway, gatos de raça, filhote de gato, filhote de gato persa, gatos da raça persa, gato persa, gato himalaio, gato exótico, criador de gatos, filhote, gatinho, criador de gato em ES, criador, felinos, gato persa Vitória ES, gato persa Espírito Santo">
    <meta name="description" content="Gatil Neway - Especializado em Persas, Himalaias e Exóticos, localizado em  Vitória - ES.">
    <meta name="author" content="Raphael Lorencini">
    <meta name="format-detection" content="telephone=no" />
    <meta name="robots" content="index,follow">

    <!--CSS-->
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}" >
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('fonts/font-awesome.css')}}">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{URL::asset('css/ie10-viewport-bug-workaround.css')}}" rel="stylesheet">

    <script src="{{URL::asset('js/ie-emulation-modes-warning.js')}}"></script>
    <!--[if lt IE 9]><script src="{{URL::asset('js/ie8-responsive-file-warning.js')}}"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{URL::asset('js/html5shiv.js')}}"></script>
    <script src="{{URL::asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body>
@include('menu')

@yield('content')

<!--footer-->
<div class="container">
    <footer>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 rodape1">
                <p>
                    <img src="{{URL::asset('img/foo_logo2.png')}}" alt=""><span><em id="copyright-year"></em> &copy; Gatil Neway</span>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 rodape2">
                @include('includes.social')
            </div>
        </div>
    </footer>
</div>
<!--JS-->
<script src="{{URL::asset('js/jquery.js')}}"></script>
<script src="{{URL::asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{URL::asset('js/superfish.js')}}"></script>
<script src="{{URL::asset('js/jquery.easing.1.3.js')}}"></script>
<script src="{{URL::asset('js/jquery.mobilemenu.js')}}"></script>
<script src="{{URL::asset('js/jquery.ui.totop.js')}}"></script>
<script src="{{URL::asset('js/jquery.equalheights.js')}}"></script>
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/tm-scripts.js')}}"></script>
<script src="{{URL::asset('js/scripts.js')}}"></script>

@yield('scripts')
</body>
</html>