<!-- CKEditor -->
<script src="{{ URL::asset('js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('js/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ URL::asset('js/ckeditor/ckeditor-init.js') }}"></script>
@if(isset($ckeditor_id) && !empty($ckeditor_id))
    <script>
        ckeditor('{{$ckeditor_id}}');
    </script>
@endif
