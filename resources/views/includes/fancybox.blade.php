<!-- Add fancyBox -->
<link rel="stylesheet" href="{{ URL::asset('js/fancybox/jquery.fancybox.css?v=2.1.5') }}" type="text/css" media="screen" />
<script type="text/javascript" src="{{ URL::asset('js/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
<script type="text/javascript">
    $(function() {
        $(".fancybox, a[rel^=fotos_]").fancybox();
        $('a.fotos-btn-mais').click(function(){
            var id = $(this).attr('id').replace('_title','');
            $('a[rel="'+id+'"]').trigger('click');
            return false;
        });
    });
</script>