<script>
    $(function(){
        $('a.delete').click(function(){
            var confirma = confirm('Deseja excluir este registro?');
            if(confirma) {
                var url = $(this).attr('href');
                if (url != '') {
                    location.href = url;
                }
            }
            return false;
        });
    });
</script>