<!--header-->
<header id="topo">
    <div class="container">
        <a href="{{route('index')}}"><img src="{{URL::asset('img/topo.png')}}" alt="" class="img-responsive"></a>
        <div class="clearfix"></div>
    </div>
</header>
<header id="menu">
    <div class="container">
        <nav class="navbar navbar-default navbar-static-top tm_navbar clearfix" role="navigation">
            <ul class="nav sf-menu clearfix">
                <li class="active menu home"><a href="{{route('index')}}">home</a></li>
                <li class="menu sobre"><a href="{{route('site.sobre')}}">sobre</a></li>
                <li class="sub-menu menu nossos_gatos"><a href="{{route('site.nossos_gatos')}}">Nossos Gatos</a><span></span>
                    <ul class="submenu">
                        <li><a href="{{route('site.nossos_gatos')}}#matrizes">Matrizes</a></li>
                        <li><a href="{{route('site.nossos_gatos')}}#padreadores">Padreadores</a></li>
                        <li><a href="{{route('site.disponiveis')}}">Disponíveis</a></li>
                    </ul>
                </li>
                <li class="menu disponiveis"><a href="{{route('site.disponiveis')}}">Disponíveis</a></li>
                <li class="menu videos"><a href="{{route('site.videos')}}">vídeos</a></li>
                <li class="menu noticias"><a href="{{route('site.artigos')}}">artigos</a></li>
                <li class="menu acfe"><a href="{{route('site.acfe')}}">ACFe</a></li>
                <li class="menu contato"><a href="{{route('site.contato')}}">contato</a></li>
            </ul>
        </nav>
    </div>
</header>