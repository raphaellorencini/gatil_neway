@extends('admin')

@section('content')

<div class="container">
    <h3>Fotos Categorias</h3>

    <br>
    <a href="{{ route('admin.fotos_categorias.novo') }}" class="btn btn-default">Nova Categoria</a>
    <form action="{{route('admin.fotos_categorias.index')}}" method="get" class="form-inline" role="form" style="display:inline-block;">
        <div class="form-group">
            <label class="sr-only" for="">Grupo</label>
            <select class="form-control" name="grupo" id="grupo">
                <option value=""> -- Filtrar Grupo -- </option>
                <option value="1">Matrizes</option>
                <option value="2">Padreadores</option>
                <option value="3">Disponíveis</option>
                <option value="4">Galerias de Fotos</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">buscar</button>
    </form>
    <br><br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Imagem</th>
            <th>Grupo</th>
            <th>Publicado</th>
            <th width="15%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fotos_categorias as $v)
        <tr>
            <td>{{$v->nome}}</td>
            <td>
                @if(!empty($v->arquivo))
                    {!! Thumb::img($v->arquivo) !!}
                @else
                    <img src="{{URL::asset('img/img-vazio.png')}}" alt="" class="img-responsive" style="width: 200px;">
                @endif
            </td>
            <td>{{Status::label_grupo($v->grupo)}}</td>
            <td>{{Status::label_publicado($v->publicado)}}</td>
            <td>
                <a href="{{ route('admin.fotos.index',['id' => $v->id]) }}" class="btn btn-info btn-xs">fotos</a>
                <a href="{{ route('admin.fotos_categorias.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                <a href="{{ route('admin.fotos_categorias.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete">excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $fotos_categorias->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection