@extends('admin')

@section('content')

    <div class="container">
        <h3>Nova Categoria</h3>

        <br>
        <a href="{{ route('admin.fotos_categorias.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.fotos_categorias.salvar', 'class' => 'form']) !!}

        @include('admin.fotos_categorias.form')

        {!! Form::close() !!}

    </div>

@endsection