@extends('admin')

@section('content')

    <div class="container">
        <h3>@if(!empty($botoes['titulo'])) Fotos - @endif<strong>{{$foto_categoria->nome}}</strong></h3>

        <br>
        <a href="{{ route('admin.fotos.index',['foto_categoria_id' => $foto_categoria_id]) }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => ['admin.fotos.salvar2', $foto_categoria_id], 'class' => 'form']) !!}

        @include('admin.fotos.form')

        {!! Form::close() !!}

    </div>

@endsection