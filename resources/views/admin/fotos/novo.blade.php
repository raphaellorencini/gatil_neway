@extends('admin')

@section('content')

    <div class="container">
        <h3>Fotos - <strong>{{$foto_categoria->nome}}</strong></h3>

        <br>
        <a href="{{ route('admin.fotos.index',['foto_categoria_id' => $foto_categoria->id]) }}" class="btn btn-default">Voltar</a>
        <br><br>

        <div id='fotosList'>
            <table class='fotosEditor table table-striped'>
                <thead>
                <tr>
                    <th width="50%">Fotos</th>
                    <th width="45%">Legenda</th>
                    <th width="5%"></th>
                </tr>
                </thead>
                <tbody data-bind="foreach: fotos">
                <tr>
                    <td>
                        <div class="form-group input-group">
                        <input class="form-control" data-bind='value: arquivo, attr:{"id":"foto"+$index()}' />
                        <span class="input-group-btn">
                            <a data-bind='click:function(){$parent.filemanager_abrir2("foto"+$index())}' class="btn btn-default filemanager-btn" target="_blank">Selecionar Imagem</a>
                        </span>
                        </div>
                    </td>
                    <td>
                        <input class="form-control" data-bind='value: legenda' />
                    </td>
                    <td>
                        <div><a href='#' data-bind='click: $root.removeFoto' class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> remover</a></div>
                    </td>
                </tr>
                </tbody>
            </table>
            <p>
                <button data-bind='click: save' class="btn btn-sx btn-primary">Salvar</button>
                <button data-bind='click: addFoto' class="btn btn-sx btn-warning"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nova Fotos</button>
                <span class="ajax-load" style="display: none;">
                    <img src="{{URL::asset('img/ajax_load.gif')}}" alt="">
                    <strong>Salvando, aguarde!</strong>
                </span>
            </p>

            {!! Form::open(['url' => '/admin/fotos/salvar/'.$foto_categoria_id, 'method' => 'post', 'class' => 'form']) !!}
            <input type="hidden" name="fotos" id="fotos">
            {!! Form::close() !!}

        </div>
    </div>
@endsection
@section('scripts')
    @include('includes.filemanager')
    <script src="{{ URL::asset('js/knockoutjs/knockout-3.4.0.js') }}"></script>
    <script>
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var initialData = [
                {id:"", foto_categoria_id:"{{$foto_categoria->id}}", legenda:"", arquivo: ""},
                {id:"", foto_categoria_id:"{{$foto_categoria->id}}", legenda:"", arquivo: ""},
                {id:"", foto_categoria_id:"{{$foto_categoria->id}}", legenda:"", arquivo: ""},
                {id:"", foto_categoria_id:"{{$foto_categoria->id}}", legenda:"", arquivo: ""},
                {id:"", foto_categoria_id:"{{$foto_categoria->id}}", legenda:"", arquivo: ""}
            ];

            var FotosModel = function(fotos) {
                var self = this;
                self.fotos = ko.observableArray(ko.utils.arrayMap(fotos, function(foto) {
                    return {
                        id: foto.id,
                        foto_categoria_id: foto.foto_categoria_id,
                        legenda: foto.legenda,
                        arquivo: foto.arquivo
                    };
                }));

                self.filemanager_abrir2 = function(id){
                    open_popup('/js/filemanager/dialog.php?type=1&popup=1&field_id='+id+'&relative_url=1&sort_by=date&descending=1');
                };

                self.foto_categoria = ko.observable({{$foto_categoria_id}});
                self.arquivo = ko.observable();
                self.legenda = ko.observable();

                self.addFoto = function() {
                    self.fotos.push({
                        id: "",
                        foto_categoria_id: self.foto_categoria(),
                        arquivo: self.arquivo(),
                        legenda: self.legenda()
                    });
                };

                self.removeFoto = function(foto) {
                    var confirma = confirm('Deseja excluir esta foto?');
                    if (confirma) {
                        self.fotos.remove(foto);
                    }
                };

                self.save = function() {
                    $('.ajax-load').show();
                    /*var valores = JSON.stringify(ko.toJS(self.fotos,null,2));
                    console.log(valores);*/
                    var valores = JSON.stringify(ko.toJS(self.fotos));
                    $('#fotos').val(valores);
                    $('form').trigger('submit');
                };

            };

            ko.applyBindings(new FotosModel(initialData));
        });
    </script>

@endsection