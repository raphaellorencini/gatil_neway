{!! Form::token() !!}

@if(!empty($botoes['titulo']))
    <div class="form-group input-group">
        {!! Form::label('arquivo','Imagem:') !!}
        {!! Form::text('arquivo', null, ['class' => 'form-control required']) !!}
    </div>
@else
    <div class="form-group">
        {!! Form::label('arquivo','Vídeo:') !!}
        {!! Form::text('arquivo', null, ['class' => 'form-control required']) !!}
    </div>
@endif

<div class="form-group">
    {!! Form::label('legenda','Legenda:') !!}
    {!! Form::text('legenda', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
</div>

@section('scripts')
    @if(!empty($botoes['titulo']))
        @include('includes.filemanager')
        <script>
            $(function(){
                filemanager_abrir('arquivo');
            });
        </script>
    @endif
    @include('includes.validate')
@endsection