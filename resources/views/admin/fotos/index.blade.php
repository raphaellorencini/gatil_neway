@extends('admin')

@section('content')

<div class="container">
    <h3>@if(!empty($botoes['titulo'])) Fotos - @endif<strong>{{$foto_categoria->nome}}</strong></h3>

    <br>
    <a href="{{ route('admin.fotos.novo2',['id' => $foto_categoria_id]) }}" class="btn btn-default">{{$botoes['novo']}}</a>
    @if(!empty($botoes['voltar']))
    <a href="{{ route('admin.fotos_categorias.index') }}" class="btn btn-default">{{$botoes['voltar']}}</a>
    @endif
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>{{$botoes['titulo2']}}</th>
            <th>Legenda</th>
            <th width="15%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fotos as $v)
        <tr>
            <td>
                @if(strpos($v->arquivo,'youtube') === false)
                    {!! Thumb::img($v->arquivo) !!}
                @else
                    {!! Youtube::thumb($v->arquivo,'mq') !!}
                @endif
            </td>
            <td>{{$v->legenda}}</td>
            <td>
                <a href="{{ route('admin.fotos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                <a href="{{ route('admin.fotos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete">excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $fotos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection