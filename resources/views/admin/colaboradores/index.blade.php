@extends('admin')

@section('content')

<div class="container">
    <h3>Colaboradores</h3>

    <br>
    <a href="{{ route('admin.colaboradores.novo') }}" class="btn btn-default">Novo Colaborador</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Imagem</th>
            <th>Nome</th>
            <th width="10%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($colaboradores as $v)
        <tr>
            <td>{!! Thumb::img($v->arquivo,200) !!}</td>
            <td>{{$v->nome}}</td>
            <td>
                <a href="{{ route('admin.colaboradores.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                <a href="{{ route('admin.colaboradores.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete">excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $colaboradores->render() !!}
</div>

@endsection
@section('scripts')
    @include('includes.delete_alert')
@endsection