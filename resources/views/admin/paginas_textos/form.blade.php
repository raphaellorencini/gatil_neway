{!! Form::token() !!}

<div class="form-group">
    {!! Form::label('nome','Título:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto','Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
</div>

@section('scripts')
    @include('includes.filemanager')
    @include('includes.ckeditor',['ckeditor_id' => 'texto'])
    <script>
        $(function(){
            filemanager_abrir('arquivo');
        });
    </script>
    @include('includes.validate')
@endsection