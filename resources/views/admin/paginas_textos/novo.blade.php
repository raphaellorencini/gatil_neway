@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Texto</h3>

        <br>
        <a href="{{ route('admin.paginas_textos.index',['pagina_id' => $pagina_id]) }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => ['admin.paginas_textos.salvar', 'pagina_id' => $pagina_id], 'class' => 'form']) !!}

        @include('admin.paginas_textos.form')

        {!! Form::close() !!}

    </div>

@endsection