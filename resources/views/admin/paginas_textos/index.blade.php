@extends('admin')

@section('content')

<div class="container">
    <h3><strong>{{$pagina->nome}}</strong> - Textos</h3>

    <br>
    <a href="{{ route('admin.paginas_textos.novo',['pagina_id' => $pagina->id]) }}" class="btn btn-default">Novos Textos</a>
    <a href="{{ route('admin.paginas.index') }}" class="btn btn-default">Voltar para Páginas</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Título</th>
            <th>Texto</th>
            <th width="15%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paginas_textos as $v)
        <tr>
            <td>{{$v->nome}}</td>
            <td>{{substr(strip_tags($v->texto),0,30)}}...</td>
            <td>
                <a href="{{ route('admin.paginas_textos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                <a href="{{ route('admin.paginas_textos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete">excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $paginas_textos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection