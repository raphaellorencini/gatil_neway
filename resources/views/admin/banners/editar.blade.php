@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Banner: {{ $banner->nome }}</h3>

        <br>
        <a href="{{ route('admin.banners.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($banner, ['route' => ['admin.banners.atualizar', $banner->id], 'class' => 'form']) !!}

        @include('admin.banners.form')

        {!! Form::close() !!}

    </div>

@endsection