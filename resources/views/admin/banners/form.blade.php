{!! Form::token() !!}
<div class="form-group input-group">
    {!! Form::label('arquivo','Imagem:') !!}
    {!! Form::text('arquivo', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('legenda','Legenda:') !!}
    {!! Form::text('legenda', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
</div>

@section('scripts')
    @include('includes.filemanager')
    <script>
        $(function(){
            filemanager_abrir('arquivo');
        });
    </script>
    @include('includes.validate')
@endsection