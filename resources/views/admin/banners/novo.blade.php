@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Banner</h3>

        <br>
        <a href="{{ route('admin.banners.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.banners.salvar', 'class' => 'form']) !!}

        @include('admin.banners.form')

        {!! Form::close() !!}

    </div>

@endsection