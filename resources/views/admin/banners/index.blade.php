@extends('admin')

@section('content')

<div class="container">
    <h3>Banners</h3>

    {{--<br>
    <a href="{{ route('admin.banners.novo') }}" class="btn btn-default">Novo Banner</a>--}}
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Banner</th>
            <th>Legenda</th>
            <th width="10%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($banners as $v)
        <tr>
            <td>{!! Thumb::img($v->arquivo) !!}</td>
            <td>{{$v->legenda}}</td>
            <td>
                <a href="{{ route('admin.banners.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $banners->render() !!}
</div>

@endsection