@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Usuário</h3>

        <br>
        <a href="{{ route('admin.users.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.users.salvar', 'class' => 'form']) !!}

        @include('admin.users.form',['tipo' => 'novo'])

        {!! Form::close() !!}

    </div>

@endsection