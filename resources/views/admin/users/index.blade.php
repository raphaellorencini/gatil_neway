@extends('admin')

@section('content')

<div class="container">
    <h3>Usuários</h3>

    <br>
    <a href="{{ route('admin.users.novo') }}" class="btn btn-default">Novo Usuário</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th width="35%">Nome</th>
            <th>E-mail</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $v)
        <tr>
            <td>{{$v->name}}</td>
            <td>{{$v->email}}</td>
            <td>
                <a href="{{ route('admin.users.senha_editar',['id' => $v->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-unlock" aria-hidden="true"></i> trocar senha</a>
                <a href="{{ route('admin.users.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                @if($v->id > 1)
                <a href="{{ route('admin.users.delete',['id' => $v->id]) }}" id="delete_{{$v->id}}" class="btn btn-danger btn-xs delete">excluir</a>
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $users->render() !!}
</div>

@endsection
@section('scripts')
    @include('includes.delete_alert')
@endsection