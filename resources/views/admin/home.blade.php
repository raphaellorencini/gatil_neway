@extends('admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Painel</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="botoes-home">
                                <a href="{{ route('admin.paginas.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-file-text" aria-hidden="true"></i><br>
                                    Páginas
                                </a>
                                <a href="{{ route('admin.banners.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i><br>
                                    Banners
                                </a>
                                <a href="{{ route('admin.fotos_categorias.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-camera" aria-hidden="true"></i><br>
                                    Fotos
                                </a>
                                <a href="{{ route('admin.fotos.index',['foto_categoria_id' => 12]) }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-youtube-square" aria-hidden="true"></i><br>
                                    Vídeos
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="botoes-home">
                                <a href="{{ route('admin.artigos.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-comments-o" aria-hidden="true"></i><br>
                                    Artigos
                                </a>
                                <a href="{{ route('admin.colaboradores.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-child" aria-hidden="true"></i><br>
                                    Colaboradores
                                </a>
                                <a href="{{ route('admin.depoimentos.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-list" aria-hidden="true"></i><br>
                                    Depoimentos
                                </a>
                                <a href="{{ route('admin.users.index') }}" class="btn btn-default btn-lg">
                                    <i class="fa fa-users" aria-hidden="true"></i><br>
                                    Usuários
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
