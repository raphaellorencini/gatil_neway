@extends('admin')

@section('content')

<div class="container">
    <h3>Páginas</h3>

    <br>
    <a href="{{ route('admin.paginas.novo') }}" class="btn btn-default">Nova Página</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Título</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paginas as $v)
        <tr>
            <td>{{$v->nome}}</td>
            <td>
                <a href="{{ route('admin.paginas_textos.index',['pagina_id' => $v->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-file-text-o" aria-hidden="true"></i> textos</a>
                <a href="{{ route('admin.paginas.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                <a href="{{ route('admin.paginas.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete">excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $paginas->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection