@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Página:</h3>

        <br>
        <a href="{{ route('admin.paginas.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($pagina, ['route' => ['admin.paginas.atualizar', $pagina->id], 'class' => 'form']) !!}

        @include('admin.paginas.form')

        {!! Form::close() !!}

    </div>
@endsection