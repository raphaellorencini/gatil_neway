@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Depoimento: {{ $depoimento->nome }}</h3>

        <br>
        <a href="{{ route('admin.depoimentos.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($depoimento, ['route' => ['admin.depoimentos.atualizar', $depoimento->id], 'class' => 'form']) !!}

        @include('admin.depoimentos.form')

        {!! Form::close() !!}

    </div>

@endsection