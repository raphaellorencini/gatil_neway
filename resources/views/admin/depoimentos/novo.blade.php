@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Depoimento</h3>

        <br>
        <a href="{{ route('admin.depoimentos.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.depoimentos.salvar', 'class' => 'form']) !!}

        @include('admin.depoimentos.form')

        {!! Form::close() !!}

    </div>

@endsection