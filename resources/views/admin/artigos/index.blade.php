@extends('admin')

@section('content')

<div class="container">
    <h3>Artigos</h3>

    <br>
    <a href="{{ route('admin.artigos.novo') }}" class="btn btn-default">Novo Artigo</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Título</th>
            <th>Imagem</th>
            <th>Data</th>
            <th>Publicado</th>
            <th width="15%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($artigos as $v)
        <tr>
            <td>{{$v->titulo}}</td>
            <td>
                @if(!empty($v->arquivo))
                    {!! Thumb::img($v->arquivo) !!}
                @else
                    <img src="{{URL::asset('img/img-vazio.png')}}" alt="" class="img-responsive" style="width: 200px;">
                @endif
            </td>
            <td>{{Formata::date($v->created_at)}}</td>
            <td>{{Status::label_publicado($v->publicado)}}</td>
            <td>
                <a href="{{ route('admin.artigos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs">editar</a>
                <a href="{{ route('admin.artigos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete">excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $artigos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection