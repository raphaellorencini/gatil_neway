@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Artigo:</h3>

        <br>
        <a href="{{ route('admin.artigos.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($artigo, ['route' => ['admin.artigos.atualizar', $artigo->id], 'class' => 'form']) !!}

        @include('admin.artigos.form')

        {!! Form::close() !!}

    </div>

@endsection