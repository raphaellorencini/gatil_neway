@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Artigo</h3>

        <br>
        <a href="{{ route('admin.artigos.index') }}" class="btn btn-default">Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.artigos.salvar', 'class' => 'form']) !!}

        @include('admin.artigos.form')

        {!! Form::close() !!}

    </div>

@endsection