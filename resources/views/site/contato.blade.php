@extends('layout')

@section('content')
    <div class="global indent">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <figure class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1213.0411054249778!2d-40.33056513173345!3d-20.319702103725255!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc018fc0359dfb766!2sGatil+Neway!5e0!3m2!1spt-BR!2sbr!4v1474506600227" style="border:0" allowfullscreen></iframe>
                    </figure>
                </div>
            </div>
        </div>
        <div class="formBox">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h2>Endereço</h2>
                        <div class="info">
                            <h3>Endereço:</h3>
                            <p>
                                Cidade de Vitória - Espírito Santo
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h2>&nbsp;</h2>
                        <div class="info">
                            <h3>Telefones:</h3>
                            <p>
                                (27) 3233-3951<br>
                                (27) 99292-3399
                                <span><img src="{{URL::asset('img/ico_claro.png')}}" alt="Claro" title="Claro" style="width: 24px;"></span>
                                <span><img src="{{URL::asset('img/ico_whatsapp.png')}}" alt="Whatsapp" title="Whatsapp" style="width: 24px;"></span>
                            </p>
                        </div>
                    </div>
                    {{--<div class="col-lg-8 col-md-8 col-sm-8">
                        <a id="formulario"></a><h2>Formulário</h2>
                        <form id="contact-form" action="{{route('site.contato_enviar')}}" method="post">
                            {!! Form::token() !!}

                            @if(!empty(Session::get('enviado')))
                            <div class="btns">
                                <p>{{Session::get('enviado')}}</p>
                            </div>
                            <br style="clear: both">
                            @endif

                            <div class="holder">
                                <div class="form-div-1 clearfix">
                                    <p>Nome*</p>
                                    <label class="name">
                                        <input name="nome" type="text" placeholder="" class="required" />
                                        <span class="error-message"></span>
                                    </label>
                                </div>
                                <div class="form-div-2 clearfix">
                                    <p>E-mail*</p>
                                    <label class="email">
                                        <input name="email" type="text" placeholder="" class="required email" />
                                        <span class="error-message"></span>
                                    </label>
                                </div>
                                <div class="form-div-3 clearfix">
                                    <p>Telefone</p>
                                    <label class="phone notRequired">
                                        <input name="telefone" type="text" placeholder=""/>
                                        <span class="error-message"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-div-4 clearfix">
                                <p>Mensagem*</p>
                                <label class="message">
                                    <textarea name="mensagem" placeholder="" class="required"></textarea>
                                    <span class="error-message"></span>
                                </label>
                            </div>
                            <div class="btns">
                                <button type="submit" class="btn btn-default btn1 contato-btn">enviar</button>
                                <p>* campos obrigatórios</p>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery-validation/localization/messages_pt_BR.min.js') }}"></script>
    <script>
        $(function(){
            menu('contato');
            var maps_id = ".map";
            $('.map iframe').css("pointer-events", "none");
            $(maps_id).click(function () {
                $('.map iframe').css("pointer-events", "auto");
            });

            $(maps_id).mouseleave(function() {
                $('.map iframe').css("pointer-events", "none");
            });
            $.validator.addClassRules({
                required: {
                    required: true
                },
                minlength: {
                    minlength: 3
                },
                currency: {
                    required: true
                },
                email:{
                    email: true
                }
            });
            $('#contact-form').validate({
                //debug: true,
                errorClass:'error-message',
                validClass:'success',
                errorElement:'label',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div.form-div-4").addClass("error-message has-error");

                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".error-message").removeClass("error-message has-error");
                }
            });
        });
    </script>
@endsection