@extends('layout')

@section('content')
    <div class="global indent gallery-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 artigos">
                    <h2 class="normal">{{$texto[0]['nome']}}</h2>
                    {!! $texto[0]['texto'] !!}
                    <br><br>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function(){
            menu('acfe');
        });
    </script>
@endsection