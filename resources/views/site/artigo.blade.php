@extends('layout')

@section('content')
    <div class="global indent gallery-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 artigos">
                    <h2>{{$artigo->titulo}}</h2>
                    <time datetime="{{substr($artigo->created_at,0,10)}}">{{Formata::date(substr($artigo->created_at,0,10),'.')}}</time>
                    @if(!empty($artigo->arquivo))
                    <figure><img src="{{URL::asset($artigo->arquivo)}}" alt="" class="img-responsive"></figure>
                    @endif
                    <br>
                    <div class="texto">{!! $artigo->texto !!}</div>
                    <br><br>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function(){
            menu('noticias');
        });
    </script>
@endsection