@extends('layout')

@section('content')
    <div class="global indent gallery-box">
        <div class="container">
            <h2>Últimos Artigos</h2>
            <div class="row">

                @foreach($artigos as $v)
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumb-pad5 maxheight">
                        <div class="thumbnail">
                            <figure>
                                @if(!empty($v->arquivo))
                                    <a href="{{route('site.artigo',['slug'=>$v->slug])}}"><img src="{{URL::asset($v->arquivo)}}" alt=""></a>
                                @else
                                    <a href="{{route('site.artigo',['slug'=>$v->slug])}}"><img src="{{URL::asset('img/img-vazio.png')}}" alt=""></a>
                                @endif
                            </figure>
                            <div class="caption">
                                <time datetime="{{substr($v->created_at,0,10)}}">{{Formata::date(substr($v->created_at,0,10),'.')}}</time>
                                <a href="{{route('site.artigo',['slug'=>$v->slug])}}" class="description">{{$v->titulo}}</a>
                                <p>{{Texto::resumo($v->texto)}}...</p>
                                <a href="{{route('site.artigo',['slug'=>$v->slug])}}" class="btn-default btn1">mais...</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="clearfix"></div>
                <div class="col-lg-8 col-md-8 col-sm-8 centre-box">
                    <h2>Mais Artigos</h2>
                    @foreach($artigos2 as $v)
                    <time datetime="{{substr($v->created_at,0,10)}}"><a href="{{route('site.artigo',['slug'=>$v->slug])}}">{{Formata::date(substr($v->created_at,0,10),'.')}}</a></time>
                    <br>
                    <a href="{{route('site.artigo',['slug'=>$v->slug])}}" class="title">{{$v->titulo}}</a>
                    <p>{{Texto::resumo($v->texto)}}...</p>
                    @endforeach
                    <div>{!! $artigos2->render() !!}</div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <h2>Arquivo</h2>
                    <ul class="list3">
                        <li><a href="#">2016</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function(){
            menu('noticias');
        });
    </script>
@endsection