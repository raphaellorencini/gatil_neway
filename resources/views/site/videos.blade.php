@extends('layout')

@section('content')
    <div class="global indent">
        <!--content-->
        <div class="container">
            <h2>Vídeos</h2>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    <div class="row">
                        @foreach($videos as $v)
                            <div class="col-lg-6 col-md-6 col-sm-6 col-sx-12">
                                <div class="thumb-pad3">
                                    <div class="thumbnail">
                                        <div class="caption">
                                            <a href="#" class="title">{{$v->legenda}}</a>
                                            <p>{!! $v->arquivo !!}</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        {!! $videos->render() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('includes.fancybox')
    <script>
        $(function(){
            menu('videos');
        });
    </script>
@endsection