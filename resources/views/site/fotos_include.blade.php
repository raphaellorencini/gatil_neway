<?php
if(!empty($col_tamanho)){
    $col = $col_tamanho;
}else{
    $col = 6;
}
?>
@if(count($valores) > 0)
<a id="{{$ancora}}"></a><h3>{!! $titulo !!}</h3>
<div class="row">
    @foreach($valores as $v)
        <div class="col-lg-{{$col}} col-md-{{$col}} col-sm-12 col-sx-12">
            <div class="thumb-pad3">
                <div class="thumbnail">
                    <div class="caption">
                        <a href="#" id="fotos_title_{{$v->id}}" class="title fotos-btn-mais">{{$v->nome}}</a>
                    </div>
                    <figure>
                        <a href="{{$v->arquivo}}" rel="fotos_{{$v->id}}" class="title" title="{{$v->nome}}">{!!Thumb::img($v->arquivo)!!}</a>
                    </figure>
                </div>
                <div>
                    <div class="caption">
                        <p>{{$v->descricao}}</p>
                        <a href="#" id="fotos_{{$v->id}}" class="btn-default btn1 btn-mais fotos-btn-mais">mais fotos...</a>
                    </div>
                </div>
                @foreach($v->fotos as $v2)
                    @if(strpos($v2->arquivo,'youtube') === false)
                        <a href="{{$v2->arquivo}}" rel="fotos_{{$v->id}}" class="title" title="{{$v2->legenda}}"></a>
                    @else
                        <a href="{{Youtube::embed($v2->arquivo)}}" rel="fotos_{{$v->id}}" class="title fancybox.iframe" title="{{$v2->legenda}}"></a>
                    @endif
                @endforeach
            </div>
        </div>
    @endforeach
</div>
@endif