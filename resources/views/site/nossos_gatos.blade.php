@extends('layout')

@section('content')
    <div class="global indent">
        <!--content-->
        <div class="container">
            <h2>Nossos Gatos</h2>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    @include('site.fotos_include',['titulo'=> 'Matrizes', 'valores' => $matrizes, 'ancora' => 'matrizes'])
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    @include('site.fotos_include',['titulo'=> 'Padreadores', 'valores' => $padreadores, 'ancora' => 'padreadores'])
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    @include('site.fotos_include',['titulo'=> 'Disponíveis (<a href="'.route('site.disponiveis').'#comprar">Clique aqui para transferência de valores</a>)', 'valores' => $disponiveis, 'ancora' => 'disponiveis'])
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    @include('site.fotos_include',['titulo'=> 'Galeria de Fotos', 'valores' => $galeria, 'ancora' => 'galeria'])
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('includes.fancybox')
    <script>
        $(function(){
            menu('nossos_gatos');
        });
    </script>
@endsection