@extends('layout')

@section('content')
    <div class="global indent">
        <!--content-->
        <div class="container">
            <h2>Disponíveis</h2>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    {!! $texto[0]['texto'] !!}
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box">
                    @include('site.fotos_include',['titulo'=> '', 'valores' => $disponiveis, 'ancora' => 'disponiveis', 'col_tamanho' => 4])
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services-box" style="text-align: center ">
                    <hr>
                    <h2>Transferência de Valores</h2>
                    <p>
                        Informe o valor combinado abaixo para concluir a transferência de valores.
                    </p>
                    <div>
                        <img src="https://stc.pagseguro.uol.com.br/public/img/banners/pagamento/todos_estatico_550_100.gif" alt="Logotipos de meios de pagamento do PagSeguro" title="Este site aceita pagamentos com Visa, MasterCard, Diners, American Express, Hipercard, Aura, Elo, PLENOCard, PersonalCard, BrasilCard, FORTBRASIL, Cabal, Mais!, Avista, Grandcard, Sorocred, Bradesco, Itaú, Banco do Brasil, Banrisul, Banco HSBC, saldo em conta PagSeguro e boleto."><br><br>
                    </div>
                    <!-- Declaração do formulário -->
                    <a id="comprar"></a>
                    <form id="form_pagseguro" method="post" target="pagseguro" action="https://pagseguro.uol.com.br/v2/checkout/payment.html">
                        <div class="form-group" style="width: 250px; display: inline-block;">
                            <label class="sr-only" for="">Valor</label>
                            <input type="text" class="form-control currency" id="valor" placeholder="Digite o valor combinado aqui">
                        </div>
                        <br>
                        <!-- Campos obrigatórios -->
                        <input name="receiverEmail" type="hidden" value="lilian.lp.santos@gmail.com">
                        <input name="currency" type="hidden" value="BRL">

                        <!-- Itens do pagamento (ao menos um item é obrigatório) -->
                        <input name="itemId1" type="hidden" value="0001">
                        <input name="itemDescription1" type="hidden" value="Filhote Persa">
                        <input name="itemAmount1" type="hidden" value="2500">
                        <input name="itemQuantity1" type="hidden" value="1">
                        <input name="itemWeight1" type="hidden" value="1000">

                        <!-- submit do form (obrigatório) -->
                        <input alt="Pague com PagSeguro" name="submit"  type="image" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar.gif"/>

                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/autonumeric/autoNumeric-min.js').'?r='.rand() }}"></script>
    @include('includes.fancybox')
    <script>
        $(function(){
            $('#valor').change(function(){
                var valor = $(this).val().replace('.','').replace('.','').replace('.','').replace(',','.');
                $('input[name="itemAmount1"]').val(valor);
            });
            $('#form_pagseguro').submit(function(){
                var id = 'input[name="itemAmount1"]';
                var valor = $(id).val();
                if(valor == '' || valor == 0){
                    alert('Preencha o valor combinado!');
                    $(id).focus();
                    return false;
                }
                return true;
            });

            $('input.currency').autoNumeric("init",{
                aSep: '.',
                aDec: ',',
                aSign: ''
            });

            menu('disponiveis');
        });
    </script>
@endsection