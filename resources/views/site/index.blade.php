@extends('layout')

@section('content')
    <div class="global">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="slider">
                        <div class="camera_wrap">
                            @foreach($banners as $v)
                            <div data-src="{{$v->arquivo}}"><div class="camera-caption fadeIn"><p class="title">{{$v->legenda}}</p></div></div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumb-pad0">
                        <div class="thumbnail">
                            <figure><img src="{{URL::asset('img/desenho01.png')}}" alt="" style="width:127px"></figure>
                            <div class="caption">
                                <a href="#" class="title">Gatos trazem alegria<br>à nossas vidas <br></a>
                                <p>Os gatos são animais ótimos de convívio, sossegados, independentes, muito carinhosos e higiênicos..</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumb-pad0">
                        <div class="thumbnail">
                            <figure><img src="{{URL::asset('img/desenho02.png')}}" alt="" style="width:127px"></figure>
                            <div class="caption">
                                <a href="#" class="title">Os gatos fazem você <br>se sentir especial!</a>
                                <p>O gato se contenta em ficar ao seu lado, mesmo enquanto você está ocupado com outras coisas.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="thumb-pad0">
                        <div class="thumbnail">
                            <figure><img src="{{URL::asset('img/desenho03.png')}}" alt="" style="width:127px"></figure>
                            <div class="caption">
                                <a href="#" class="title">São divertidos <br>e melhoram seu humor!</a>
                                <p>Os estudos comprovam que pessoas que têm animais de estimação tendem a apresentar e demonstrar menos estresse quando comparadas com aquelas que não têm animais.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="about-box">
                    <div class="col-lg-7 col-md-7 col-sm-12">
                        <h2 class="indent"><span>{{$texto[0]['nome']}}</span></h2>
                        <div class="thumb-pad1">
                            <div class="thumbnail">
                                <figure><img src="{{URL::asset('img/saiba_tudo.jpg')}}" alt="" style="width: 270px;"></figure>
                                <div class="caption">
                                    {!! $texto[0]['texto'] !!}
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-lg-offset-1 pets-box">
                        <h2>Nossos Gatos</h2>
                        <ul class="list2">

                            @foreach($matrizes as $v)
                                <li>
                                    <figure>
                                        <a href="{{$v->arquivo}}" rel="fotos_{{$v->id}}" class="title" title="{{$v->nome}}">{!!Thumb::img($v->arquivo, 100)!!}</a>
                                    </figure>
                                    <div class="extra-wrap">
                                        <a href="#" class="title" title="{{$v->nome}}">{{$v->nome}}</a>
                                        <p>{{$v->descricao}}</p>
                                    </div>
                                    @foreach($v->fotos as $v2)
                                        <a href="{{$v2->arquivo}}" rel="fotos_{{$v->id}}" class="title {{$v2->classe_css}}" title="{{$v2->legenda}}"></a>
                                    @endforeach
                                </li>
                            @endforeach
                            @foreach($padreadores as $v)
                                <li>
                                    <figure>
                                        <a href="{{$v->arquivo}}" rel="fotos_{{$v->id}}" class="title" title="{{$v->nome}}">{!!Thumb::img($v->arquivo, 100)!!}</a>
                                    </figure>
                                    <div class="extra-wrap">
                                        <a href="#" class="title" title="{{$v->nome}}">{{$v->nome}}</a>
                                        <p>{{$v->descricao}}</p>
                                    </div>
                                    @foreach($v->fotos as $v2)
                                        <a href="{{$v2->arquivo}}" rel="fotos_{{$v->id}}" class="title" title="{{$v2->legenda}}"></a>
                                    @endforeach
                                </li>
                            @endforeach
                        </ul>
                        <a href="{{route('site.nossos_gatos')}}" class="btn-default btn1">mais...</a>
                    </div>
                </div>

                <div class="col-lg-12">
                    <h2 class="indent"><span>Disponíveis</span></h2>
                    <div class="thumb-pad1">
                        <div class="thumbnail">
                            @include('site.fotos_include',['titulo'=> '', 'valores' => $disponiveis, 'ancora' => 'disponiveis', 'col_tamanho' => 4])
                            <p>
                                <span>Todos são alimentados com ração Premier.</span><br>
                                <a href="http://www.premierpet.com.br" target="_blank" style="display: inline-block;"><img src="{{URL::asset('img/premier.png')}}" alt=""></a>
                            </p>
                            <a href="{{route('site.disponiveis')}}" class="btn-default btn1">mais disponíveis...</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <h2 class="indent"><span>{{$texto[1]['nome']}}</span></h2>
                    <div class="thumb-pad1">
                        <div class="thumbnail">
                            <figure><img src="{{URL::asset('img/nossos_servicos.jpg')}}" alt="" style="width: 270px;"></figure>
                            <div class="caption">

                                {!! $texto[1]['texto'] !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 news-box">
                    <h2>Últimos Vídeos</h2>
                    <div class="row">
                        @foreach($videos as $v)
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <p>
                                    {!! Youtube::embed($v->arquivo,true,'100%') !!}
                                </p>
                            </div>
                        @endforeach
                        <br style="clear: both">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a href="{{route('site.videos')}}" class="btn-default btn1">mais...</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 news-box">
                    <h2>Últimos Artigos</h2>
                    <div class="row">
                        @foreach($artigos as $v)
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <time datetime="{{substr($v->created_at,0,10)}}"><a href="{{route('site.artigo',['slug'=>$v->slug])}}">{{Formata::date(substr($v->created_at,0,10),'.')}}</a></time>
                            <p>
                                <a href="{{route('site.artigo',['slug'=>$v->slug])}}" class="title">{{$v->titulo}}</a><br>
                                {!! Texto::resumo($v->texto, 80) !!}...
                            </p>
                        </div>
                        @endforeach
                        <br style="clear: both">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <a href="{{route('site.artigos')}}" class="btn-default btn1">mais...</a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pets-box">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="https://www.facebook.com/gneway/?fref=ts" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/gneway/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gneway/?fref=ts">Gatil Neway</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection

@section('scripts')
    @include('includes.fancybox')
    <link rel="stylesheet" href="{{URL::asset('css/camera.css')}}">
    <script src="{{URL::asset('js/camera.js')}}"></script>
    <script>
        $(function(){
            $('.camera_wrap').camera();
            menu('home');
        });
    </script>
@endsection