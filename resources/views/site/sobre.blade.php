@extends('layout')

@section('content')
    <div class="global indent">
        <!--content-->
        <div class="who-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-6">
                        <h2>Sobre</h2>
                        <div class="thumb-pad4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <p class="title">{{$texto[0]['nome']}}</p>
                                    {!! $texto[0]['texto'] !!}
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 trainers-box">
                        <h2>Nossos Colaboradores</h2>
                        <ul class="list2 list2-sobre">
                            @foreach($colaboradores as $v)
                            <li>
                                <figure>
                                    <img src="{{URL::asset($v->arquivo)}}" alt="" style="width: 170px;">
                                </figure>
                                <div class="extra-wrap">
                                    <a href="#" class="title" title="{{$v->nome}}">{{$v->nome}}</a>
                                    <p>{!! $v->texto !!}</p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimBox">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2>Depoimentos</h2>
                        <div class="row">
                            @foreach($depoimentos as $v)
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="thumb-pad6">
                                    <div class="thumbnail">
                                        <figure><img src="{{URL::asset('img/quote.png')}}" alt=""></figure>
                                        <div class="caption">
                                            <p>
                                                {{$v->texto}}
                                            </p>
                                            <a href="#">{{$v->nome}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function(){
            menu('sobre');
        });
    </script>
@endsection